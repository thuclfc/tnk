/*!
 * 
 * 
 * 
 * @author Thuclfc
 * @version 
 * Copyright 2021. MIT licensed.
 */$(document).ready(function () {
  // show hidden menu
  $('.navbar-toggler').on('click', function () {
    $('.navbar,.modal-navbar').toggleClass('show');
  });
  $('.navbar .close,.modal-navbar').on('click', function () {
    $('.navbar,.modal-navbar').removeClass('show');
  }); // active navbar of page current

  var urlcurrent = window.location.href;
  $(".navbar-menu li a[href$='" + urlcurrent + "'],.nav-category li a[href$='" + urlcurrent + "']").addClass('active');
});